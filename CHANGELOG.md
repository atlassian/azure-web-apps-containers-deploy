# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.3.2

- patch: Internal maintenance: Bump base docker image to 2.70.0.
- patch: Internal maintenance: Update pipes versions in pipelines configuration file.

## 1.3.1

- patch: Internal maintenance: Bump pipes versions in pipelines configuration file.
- patch: Internal maintenance: Update docker image to azure-cli:2.67.0

## 1.3.0

- minor: Bump azure-cli to version 2.63.0.
- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 1.2.0

- minor: Add feature to change default Azure cloud to government cloud.
- patch: Internal maintenance: update azure-cli image and bitbucket-pipes-toolkit-bash.
- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update default image, pipe versions in bitbucket-pipelines.yml.

## 1.1.0

- minor: Support AZURE_SUBSCRIPTION selection when having multiple subscriptions on service principal.

## 1.0.1

- patch: Internal maintenance: make pipe structure consistent to others.

## 1.0.0

- major: This pipe was forked from https://bitbucket.org/microsoft/azure-web-apps-containers-deploy for future maintenance purpose.
