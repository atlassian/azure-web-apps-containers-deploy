#!/bin/bash

# Deploy Container with Web App for Containers
# https://docs.microsoft.com/en-us/azure/app-service/containers/tutorial-custom-docker-image
#
# Required globals:
#   AZURE_APP_ID
#   AZURE_PASSWORD
#   AZURE_TENANT_ID
#   AZURE_APP_NAME
#   AZURE_RESOURCE_GROUP
#   DOCKER_CUSTOM_IMAGE_NAME
#
# Optional globals:
#   DOCKER_REGISTRY_SERVER_URL
#   DOCKER_REGISTRY_SERVER_USER
#   DOCKER_REGISTRY_SERVER_PASSWORD
#   SLOT
#   EXTRA_ARGS
#   DEBUG

source "$(dirname "$0")/common.sh"

enable_debug

# mandatory parameters
AZURE_APP_ID=${AZURE_APP_ID:?'AZURE_APP_ID variable missing.'}
AZURE_PASSWORD=${AZURE_PASSWORD:?'AZURE_PASSWORD variable missing.'}
AZURE_TENANT_ID=${AZURE_TENANT_ID:?'AZURE_TENANT_ID variable missing.'}
AZURE_APP_NAME=${AZURE_APP_NAME:?'AZURE_APP_NAME variable missing.'}
AZURE_RESOURCE_GROUP=${AZURE_RESOURCE_GROUP:?'AZURE_RESOURCE_GROUP variable missing.'}
DOCKER_CUSTOM_IMAGE_NAME=${DOCKER_CUSTOM_IMAGE_NAME:?'DOCKER_CUSTOM_IMAGE_NAME variable missing.'}

AZURE_SUBSCRIPTION=${AZURE_SUBSCRIPTION:=''}

debug AZURE_APP_ID: "${AZURE_APP_ID}"
debug AZURE_TENANT_ID: "${AZURE_TENANT_ID}"
debug AZURE_RESOURCE_GROUP: "${AZURE_RESOURCE_GROUP}"
debug AZURE_APP_NAME: "${AZURE_APP_NAME}"
debug DOCKER_CUSTOM_IMAGE_NAME: "${DOCKER_CUSTOM_IMAGE_NAME}"


# auth
AUTH_ARGS_STRING="--username ${AZURE_APP_ID} --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}"

if [[ "${DEBUG}" == "true" ]]; then
  AUTH_ARGS_STRING="${AUTH_ARGS_STRING} --debug"
fi

# If cloud name is provided set that value to be the default cloud.

if [[ -n "${AZURE_CLOUD_ENVIRONMENT}" ]]; then

  run az cloud set --name "${AZURE_CLOUD_ENVIRONMENT}"

  info "Changing Cloud ..."

fi

AUTH_ARGS_STRING="${AUTH_ARGS_STRING} ${EXTRA_ARGS:=""}"

debug AUTH_ARGS_STRING: "${AUTH_ARGS_STRING}"

info "Signing in..."
run az login --service-principal ${AUTH_ARGS_STRING}

if [ -n "${AZURE_SUBSCRIPTION}" ]; then
  info "Setting the subscription..."
  az account set --subscription="${AZURE_SUBSCRIPTION}"
fi

# deployment
ARGS_STRING="--resource-group ${AZURE_RESOURCE_GROUP} --name ${AZURE_APP_NAME} --docker-custom-image-name ${DOCKER_CUSTOM_IMAGE_NAME}"

if [[ -n "${DOCKER_REGISTRY_SERVER_URL}" ]]; then
  ARGS_STRING="${ARGS_STRING} --docker-registry-server-url ${DOCKER_REGISTRY_SERVER_URL}"
fi

if [[ -n "${DOCKER_REGISTRY_SERVER_USER}" ]]; then
  ARGS_STRING="${ARGS_STRING} --docker-registry-server-user ${DOCKER_REGISTRY_SERVER_USER}"
fi

if [[ -n "${DOCKER_REGISTRY_SERVER_PASSWORD}" ]]; then
  ARGS_STRING="${ARGS_STRING} --docker-registry-server-password ${DOCKER_REGISTRY_SERVER_PASSWORD}"
fi

if [[ -n "${SLOT}" ]]; then
  ARGS_STRING="${ARGS_STRING} --slot ${SLOT}"
fi

if [[ "${DEBUG}" == "true" ]]; then
  ARGS_STRING="${ARGS_STRING} --debug"
fi

ARGS_STRING="${ARGS_STRING} ${EXTRA_ARGS:=""}"

debug ARGS_STRING: "${ARGS_STRING}"

info "Starting deployment to Azure App Service..."

run az webapp config container set ${ARGS_STRING}

WEBAPP_URL=$(az webapp deployment list-publishing-profiles -n "${AZURE_APP_NAME}" -g "${AZURE_RESOURCE_GROUP}" --query '[0].destinationAppUrl' -o tsv)
info "Web App URL: ${WEBAPP_URL}"

if [ "${status}" -eq 0 ]; then
  success "Deployment successful."
else
  fail "Deployment failed."
fi