#!/usr/bin/env bats

setup() {
    DOCKER_IMAGE=${DOCKER_IMAGE:="test/azure-web-apps-containers-deploy"}

    # generated
    RANDOM_NUMBER=$RANDOM

    # required globals - stored in Pipelines repository variables
    AZURE_APP_ID="${AZURE_APP_ID}"
    AZURE_PASSWORD="${AZURE_PASSWORD}"
    AZURE_TENANT_ID="${AZURE_TENANT_ID}"
    WEBSITES_PORT="${WEBSITES_PORT}"

    # required globals - generated
    AZURE_RESOURCE_GROUP="test${RANDOM_NUMBER}"
    AZURE_APP_NAME="website${RANDOM_NUMBER}"
    ACR_NAME="bbacrtest${RANDOM_NUMBER}"
    # locals
    HOSTING_PLAN_NAME="${AZURE_APP_NAME}plan"
    AZURE_LOCATION="CentralUS"

    echo "Building image..."
    docker build -t ${DOCKER_IMAGE}:0.1.0 .

    echo "Creating required Azure resources"
    az login --service-principal --username ${AZURE_APP_ID} --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}
    az group create --name ${AZURE_RESOURCE_GROUP} --location ${AZURE_LOCATION}
    az appservice plan create --name ${HOSTING_PLAN_NAME} --resource-group ${AZURE_RESOURCE_GROUP} --is-linux
    az webapp create --name ${AZURE_APP_NAME} --plan ${HOSTING_PLAN_NAME} --resource-group ${AZURE_RESOURCE_GROUP} --deployment-container-image-name dummypublisher/dummyimage:dummytag 
    az webapp config appsettings set --resource-group ${AZURE_RESOURCE_GROUP} --name ${AZURE_APP_NAME} --settings WEBSITES_PORT=${WEBSITES_PORT}
}

teardown() {
    echo "Clean up Azure resources"
    az group delete --name ${AZURE_RESOURCE_GROUP} --yes
}


@test "Image in private Registry can be deployed to Azure Web App" {

    az acr create --resource-group ${AZURE_RESOURCE_GROUP} --name ${ACR_NAME} --sku Basic --admin-enabled
    DOCKER_REGISTRY_SERVER_URL="${ACR_NAME}.azurecr.io"
    DOCKER_REGISTRY_SERVER_USER=$(az acr credential show --name ${ACR_NAME} --query username | sed 's/"//g')
    DOCKER_REGISTRY_SERVER_PASSWORD=$(az acr credential show --name ${ACR_NAME} --query passwords[0].value | sed 's/"//g')

    docker login -u ${DOCKER_REGISTRY_SERVER_USER} -p ${DOCKER_REGISTRY_SERVER_PASSWORD} ${DOCKER_REGISTRY_SERVER_URL}

    docker pull alpine:3.7
    DOCKER_CUSTOM_IMAGE_NAME="${DOCKER_REGISTRY_SERVER_URL}/testalpine:v1"
    docker tag alpine:3.7 ${DOCKER_CUSTOM_IMAGE_NAME}

    docker push ${DOCKER_CUSTOM_IMAGE_NAME}


    DOCKER_REGISTRY_SERVER_URL=${DOCKER_REGISTRY_SERVER_URL}
    DOCKER_REGISTRY_SERVER_USER=${DOCKER_REGISTRY_SERVER_USER}
    DOCKER_REGISTRY_SERVER_PASSWORD=${DOCKER_REGISTRY_SERVER_PASSWORD}

    run docker run \
        -e AZURE_APP_ID="${AZURE_APP_ID}" \
        -e AZURE_PASSWORD="${AZURE_PASSWORD}" \
        -e AZURE_TENANT_ID="${AZURE_TENANT_ID}" \
        -e AZURE_APP_NAME="${AZURE_APP_NAME}" \
        -e AZURE_RESOURCE_GROUP="${AZURE_RESOURCE_GROUP}" \
        -e DOCKER_CUSTOM_IMAGE_NAME="${DOCKER_CUSTOM_IMAGE_NAME}" \
        -e DOCKER_REGISTRY_SERVER_URL="${DOCKER_REGISTRY_SERVER_URL}" \
        -e DOCKER_REGISTRY_SERVER_USER="${DOCKER_REGISTRY_SERVER_USER}" \
        -e DOCKER_REGISTRY_SERVER_PASSWORD="${DOCKER_REGISTRY_SERVER_PASSWORD}" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:0.1.0

    echo ${output}
    [ "$status" -eq 0 ]
    [[ "${output}" == *"Deployment successful."* ]]

    run az webapp config container show --name ${AZURE_APP_NAME} --resource-group ${AZURE_RESOURCE_GROUP}

    echo ${output}
    [[ "${status}" -eq 0 ]]
    [[ "${output}" == *"DOCKER_CUSTOM_IMAGE_NAME"*"DOCKER|${DOCKER_CUSTOM_IMAGE_NAME}"* ]]
    [[ "${output}" == *"DOCKER_REGISTRY_SERVER_URL"*"${DOCKER_REGISTRY_SERVER_URL}"* ]]
    [[ "${output}" == *"DOCKER_REGISTRY_SERVER_USERNAME"*"${DOCKER_REGISTRY_SERVER_USER}"* ]]
    [[ "${output}" == *"DOCKER_REGISTRY_SERVER_PASSWORD"* ]]
}

@test "DockerHub public image can be deployed to Azure Web App setting Azure subscription" {

    # required globals - fixed
    DOCKER_CUSTOM_IMAGE_NAME="csemsftatlassian/mydockerimage:v1.0.0"

    run docker run \
        -e AZURE_APP_ID="${AZURE_APP_ID}" \
        -e AZURE_PASSWORD="${AZURE_PASSWORD}" \
        -e AZURE_TENANT_ID="${AZURE_TENANT_ID}" \
        -e AZURE_APP_NAME="${AZURE_APP_NAME}" \
        -e AZURE_RESOURCE_GROUP="${AZURE_RESOURCE_GROUP}" \
        -e DOCKER_CUSTOM_IMAGE_NAME="${DOCKER_CUSTOM_IMAGE_NAME}" \
        -e AZURE_SUBSCRIPTION="${AZURE_SUBSCRIPTION}" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:0.1.0

    echo ${output}
    [ "$status" -eq 0 ]
    [[ "${output}" == *"Deployment successful."* ]]

    run az webapp config container show --name ${AZURE_APP_NAME} --resource-group ${AZURE_RESOURCE_GROUP}

    echo ${output}
    [[ "${status}" -eq 0 ]]
    [[ "${output}" == *"DOCKER_CUSTOM_IMAGE_NAME"*"DOCKER|${DOCKER_CUSTOM_IMAGE_NAME}"* ]]
}


@test "DockerHub public image can be deployed to Azure Web App" {

    # required globals - fixed
    DOCKER_CUSTOM_IMAGE_NAME="csemsftatlassian/mydockerimage:v1.0.0"

    run docker run \
        -e AZURE_APP_ID="${AZURE_APP_ID}" \
        -e AZURE_PASSWORD="${AZURE_PASSWORD}" \
        -e AZURE_TENANT_ID="${AZURE_TENANT_ID}" \
        -e AZURE_APP_NAME="${AZURE_APP_NAME}" \
        -e AZURE_RESOURCE_GROUP="${AZURE_RESOURCE_GROUP}" \
        -e DOCKER_CUSTOM_IMAGE_NAME="${DOCKER_CUSTOM_IMAGE_NAME}" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:0.1.0

    echo ${output}
    [ "$status" -eq 0 ]
    [[ "${output}" == *"Deployment successful."* ]]

    run az webapp config container show --name ${AZURE_APP_NAME} --resource-group ${AZURE_RESOURCE_GROUP}

    echo ${output}
    [[ "${status}" -eq 0 ]]
    [[ "${output}" == *"DOCKER_CUSTOM_IMAGE_NAME"*"DOCKER|${DOCKER_CUSTOM_IMAGE_NAME}"* ]]
}
